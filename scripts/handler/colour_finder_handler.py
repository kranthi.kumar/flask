class Find_Colour:
    @staticmethod
    def findcolour(data):
        data_requried = set()
        for i_data_id, i_data_value in (data['settings']['spc_settings'].items()):
            for i_data2_value in i_data_value["data"]:
                data_requried.add(i_data2_value["color"])
        data_requried = list(data_requried)
        return {"data": data_requried}

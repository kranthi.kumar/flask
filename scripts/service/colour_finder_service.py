from flask import Blueprint
from flask import request

from scripts.handler.colour_finder_handler import Find_Colour

color_finder = Blueprint('example_blueprint', __name__)

find_colour_object = Find_Colour()


@color_finder.route('/host', methods=['POST'])
def post_request():
    content = request.get_json()
    requried_data = find_colour_object.findcolour(content)
    return requried_data
